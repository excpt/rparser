from django.test import TestCase

from models import News, Category
from rparser.rparser import RParser


class MainTestCase(TestCase):
    def test_main(self):
        rparser = RParser('tests/lenta_test.json')
        rparser.process()
        self.assertEqual(News.objects.all().count(), 8)