from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=64)

    def __unicode__(self):
        return self.name

class News(models.Model):
    category = models.ForeignKey(Category)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=200)
    author = models.CharField(max_length=200)

    def __unicode__(self):
        return self.title