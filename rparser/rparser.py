from json import loads
from django.db.models.loading import get_model
from lxml import etree


class RParser(object):

    def __init__(self, schema_path):
        with open(schema_path, 'r') as schema_file:
            self._schema = loads(schema_file.read())
        try:
            model_app, model_name = self._schema['modelName'].split('.')
        except ValueError:
            raise Exception('model_fullname must be like "SomeApp.MyModel"')
        self._model = get_model(model_app, model_name)
        self._doc = etree.parse(self._schema['url'])

    def _make_fk_obj(self, model_fullname, params):
        try:
            model_app, model_name = model_fullname.split('.')
        except ValueError:
            raise Exception('model_fullname must be like "SomeApp.MyModel"')
        model = get_model(model_app, model_name)
        obj = model()
        for field, val in params.items():
            setattr(obj, field, val)
        obj.save()
        return obj

    def _parse_complex_field(self, fields, item):
        params = {}
        for field in fields:
            res = item.xpath(field['xPath'])
            if res:
                params[field['fieldName']] = res[0].text
        return params

    def _process(self):
        items = self._doc.xpath(self._schema['itemsXPath'])
        for item in items:
            db_object = self._model()
            for field in self._schema['fields']:
                if field['foreignKey']:
                    field_val = self._make_fk_obj(
                        field['modelName'],
                        self._parse_complex_field(field['fields'], item)
                    )
                else:
                    res = item.xpath(field['xPath'])
                    if not res:
                        continue
                    field_val = res[0].text
                setattr(db_object, field['fieldName'], field_val)
            db_object.save()

    def process(self):
        self._process()
