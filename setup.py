from setuptools import setup
import rparser


setup(
    author="Ilya Grinzovskiy",
    author_email="ilya.grinzovskiy@gmail.com",
    name='rparser',
    version=rparser.__version__,
    description='An Advanced Django CMS',
    platforms=['OS Independent'],
    install_requires=[
        'Django>=1.6,<1.8',
    ],
    packages=['rparser'],
    test_suite='runtests.main',
)